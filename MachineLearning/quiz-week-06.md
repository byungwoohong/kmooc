# Week 06

## Choose the best option for the blank in the following code block to complete a given module.

### Question 1. The module ```compute_loss``` is designed to compute a linear regression loss which is defined as $` L(w)=\frac{1}{n} \sum_{i=1}^n \ \Big( w\cdot x_i – y_i \Big)^2 `$ where $` x `$ is the input data, $` y `$ is the ground truth, $` n `$ is the number of data and $` w `$ is the trainable model parameter. Choose the best option for the blank.

```python
import numpy

def compute_loss(x, w, y): 
    n           =   y.shape[0]
    y_predict   =   x * w
    loss        = ### fill in the blank ###

    return loss
```

#### (1) 
```python
    loss = 1/n * (y_predict - y).T.dot(y_predict - y)
```

#### (2)
```python
    loss = 1/n * (y_predict + y).T.dot(y_predict + y)
```

#### (3)
```python
    loss = 1/n * (y_predict - y).T.dot(y_predict + y)
```

#### (4)
```python
    loss = 1/n * (y_predict + y).T.dot(y_predict - y)
```

#### (solution) (1)
```python
    loss = 1/n * (y_predict - y).T.dot(y_predict - y)
```

### Question 2. The module ```compute_gradient``` is designed to compute the gradient of linear regression loss $`L(w) = \frac{1}{n} \sum_{i=1}^n \ \Big( w\cdot x_i – y_i \Big)^2 `$ where $` x `$ is the input data, $` y `$ is the ground truth and $` n `$ is the number of data and $` w `$ is the trainable model parameter. Choose the best option for the blank.

```python
import numpy

def compute_gradient(x, w, y): 
    n           =   y.shape[0]
    y_predict   =   x * w
    grad        = ### fill in the blank ###

    return loss
```

#### (1) 
```python
    grad = 2/n * x.T.dot(x-y)
```

#### (2)
```python
    grad = 2/n * y.T.dot(y_predict-y)
```

#### (3)
```python
    grad = 2/n * x.T.dot(y_predict-y)
```

#### (4)
```python
    grad = 2/n * y_predict.T.dot(y_predict-y)
```

#### (solution) (3)
```python
    grad = 2/n * x.T.dot(y_predict-y)
```
