# Week 14

### Question 1. Let $`\nabla \mathcal{L}`$ be the gradient of the loss $`\mathcal{L}`$ with respect to the weights $`w`$. Which of the followings is the most appropriate for the gradient descent algorithm. Note that $`t`$ denotes the iteration index and $`\eta`$ denotes the step-size.

#### (1) $`w^{(t+1)} \coloneqq \eta w^{(t+1)} - \eta \nabla \mathcal{L}(w^{(t)})`$

#### (2) $`w^{(t+1)} \coloneqq \eta w^{(t+1)} + \eta \nabla \mathcal{L}(w^{(t)})`$

#### (3) $`w^{(t+1)} \coloneqq w^{(t)} - \eta \nabla \mathcal{L}(w^{(t)})`$

#### (4) $`w^{(t+1)} \coloneqq w^{(t)} + \eta \nabla \mathcal{L}(w^{(t)})`$

#### (solution) (3)

### Question 2. Which of the followings is the most inappropriate in relation to the initialization of the weights.

#### (1) the number of weights is related to their initialization

#### (2) assign all the weights with 0

#### (3) assign all the weights randomly

#### (4) solution is subject to the choice of initialization of weights in most deep learning problems 

#### (solution) (2)