## [Week 04 - Unit 01] (Matrix Multiplication)

### Question 1. Compute the multiplication $`C = A B`$ of matrices $`A = \begin{bmatrix} 1 & 6\\ 9 & 3 \end{bmatrix}`$ and $`B = \begin{bmatrix} 0 & -1\\ -1 & 2 \end{bmatrix}`$

##### (1) $`C = \begin{bmatrix} 0 & -6\\ -9 & 6 \end{bmatrix}`$

##### (2) $`C = \begin{bmatrix} -6 & 11\\ -3 & -3 \end{bmatrix}`$

##### (3) $`C = \begin{bmatrix} -9 & -3\\ 17 & 0 \end{bmatrix}`$

##### (4) $`C = \begin{bmatrix} 1 & 5\\ 8 & 5 \end{bmatrix}`$

##### (solution) (2) $`C = \begin{bmatrix} -6 & 11\\ -3 & -3 \end{bmatrix}`$


### Question 2. Compute the third power $`A^{3}`$ of matrix $`A = \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$

##### (1) $`A^{3} = \begin{bmatrix} 0&1&0\\1&1&0\\1&0&1\end{bmatrix}`$

##### (2) $`A^{3} = \begin{bmatrix} 1&1&0\\1&1&1\\0&0&1\end{bmatrix}`$

##### (3) $`A^{3} = \begin{bmatrix} 1&0&1\\0&1&1\\1&0&1\end{bmatrix}`$

##### (4) $`A^{3} = \begin{bmatrix} 1&1&1\\1&1&1\\1&1&1\end{bmatrix}`$

##### (solution) (3) $`A^{3} = \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$ = $`\begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$ = $`\begin{bmatrix} 1&0&1\\0&1&1\\1&0&1\end{bmatrix}`$


### Question 3. Choose one _wrong_ explanation for the properties of matrix multiplication. Let A, B and C be the matrices, $`A^{T}`$ be the transposed matrix of matrix A, and $`I`$ be the Identity matrix.

##### (1) $`AI = A`$ and $`IA = A`$ 

##### (2) $`A(B+C) = A(B \times C)`$ 

##### (3) $`(AB)^{T} = B^{T}A^{T}`$ 

##### (4) $`AB = BA`$ _does not hold in general_

##### (solution) (2) $`A(B+C) = AB + AC`$ 





## [Week 04 - Unit 02] (Matrix Inverses)

### Question 1. Calculate the inverse of 2x2 matrix $` A = \begin{bmatrix} 4 & 7 \\ 2 & 6 \end{bmatrix}`$.

##### (1) $`A^{-1} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$
##### (2) $`A^{-1} = \begin{bmatrix} 0.4 & -0.7 \\ -0.1 & 0.4 \end{bmatrix}`$ 
##### (3) $`A^{-1} = \begin{bmatrix} 0.2 & -0.5 \\ -0.3 & 0.2 \end{bmatrix}`$
##### (4) $`A^{-1} = \begin{bmatrix} 0.1 & -0.3 \\ -0.2 & 0.4 \end{bmatrix}`$ 
##### (solution) (1) $`A^{-1} = \frac{1}{4\times6 - 7\times2}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \frac{1}{10}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$


### Question 2. Solve the given system of equations using the inverse of matrix.
(1). $`3x + 8y = 5`$  
(2). $`4x + 11y = 7`$
$`\newline`$(Hint: $`\begin{bmatrix}3 & 8 \\ 4 & 11 \end{bmatrix}\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}5\\7\end{bmatrix} `$)


##### (1) $`x = 1 y = 1`$
##### (2) $`x = -1 y = 1`$
##### (3) $`x = -1 y = 0`$
##### (4) $`x = 1 y = 0`$
##### (solution) (2) $`x = -1 y = 1`$
First, let $`A`$ be the coefficient matrix of the system, $`X`$ be the variable matrix, and $`B`$ be the constant matrix. Then, we can represent matrix A as $`A = \begin{bmatrix}3&8\\4&11\end{bmatrix}`$, matrix X as $`X = \begin{bmatrix}x\\y\end{bmatrix}`$ and matrix B as $`B = \begin{bmatrix}5\\7\end{bmatrix}`$.
Second, we need to calculate the inverse matrix $`A^{-1}`$ of matrix $`A`$, then we have $`A^{-1} = \begin{bmatrix}11&-8\\-4&3\end{bmatrix}`$. 
Lastly, we multiply both sides of the equation by $`A^{-1}`$. 
$`(A^{-1})AX = (A^{-1})B`$
$`\begin{bmatrix}11&-8\\-4&3\end{bmatrix}\begin{bmatrix}3&8\\4&11\end{bmatrix}\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}11&-8\\-4&3\end{bmatrix}\begin{bmatrix}5\\7\end{bmatrix}`$
$`\begin{bmatrix}1&0\\0&1\end{bmatrix}\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}11(5)+(-8)7\\-4(5)+3(7)\end{bmatrix}`$
$`\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}-1\\1\end{bmatrix}`$
Therefore, $`x = -1 y = 1`$

### Question 3. Choose one wrong explanation for the properties of inverse of matrix. Let A be the matrix, $`A^{T}`$ be the transposed matrix of matrix A, and $`I`$ be the Identity matrix.
##### (1) If $`A`$ is a square with orthonormal columns, we have $`A^{-1} = A{T}`$
##### (2) If $`A`$ is a square invertible matrix , $`(A^{T})^{-1} = (A^{-1})^{T}`$
##### (3) If $`A`$ is invertible and $`B`$ is not invertible, $`(AB)^{-1} = B^{-1}A^{-1}`$
##### (4) If $`A`$ is a square invertible matrix and k $`k`$ is a positive integer, $`(A^{k})^{-1} = (A^{-1})^{k}`$
##### (solution) (3) Both A and B matrices should be square invertible matrices (of the same size) to have the property $`(AB)^{-1} = B^{-1}A^{-1}`$.

