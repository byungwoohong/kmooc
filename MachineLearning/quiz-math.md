## [Week 03 - Unit 01] (Matrices)

### Question 1. Compute the multiplication $`C = A B`$ of matrices $`A = \begin{bmatrix} 1 & 2\\ 3 & 4 \end{bmatrix}`$ and $`B = \begin{bmatrix} 4 & 3\\ 2 & 1 \end{bmatrix}`$

##### (1) $`C = \begin{bmatrix} 8 & 5\\ 20 & 13 \end{bmatrix}`$

##### (2) $`C = \begin{bmatrix} 4 & 6\\ 6 & 4 \end{bmatrix}`$

##### (3) $`C = \begin{bmatrix} 2 & 12\\ 12 & 2 \end{bmatrix}`$

##### (4) $`C = \begin{bmatrix} 5 & 5\\ 5 & 5 \end{bmatrix}`$

##### (solution) (1) $`C = \begin{bmatrix} 8 & 5\\ 20 & 13 \end{bmatrix}`$


### Question 2. Suppose the block matrix $` \begin{bmatrix} A & I\\ I & C \end{bmatrix}`$ make senses, where size of $`A`$ is $`p \times q `$. What is the dimension of $`C`$?

##### (1) $` q \times p `$

##### (2) $` p \times q `$

##### (3) $` (p+p) \times (q+q) `$

##### (4) $` (p+q) \times (p+q) `$

##### (solution) (2) $` p \times q `$


### Question 3. Compute the $`\|A\|`$ when $` A = \begin{bmatrix} 2 & 1 \\ 2 & 3 \end{bmatrix} `$.

##### (1) $` \sqrt{8} `$

##### (2) $` \sqrt{18} `$

##### (3) $` 8 `$

##### (4) $` 18 `$

##### (solution) (2) $` \sqrt{18} `$



## [Week 03 - Unit 02] (Linear Equations)

### Question 1. What is the solution for the set of linear equations $` x_1 + x_2 = 1, x_1 = -1, x_1 - x_2 = 0 `$

##### (1) $` x_1 = -1, x_2 = 2 `$

##### (2) $` x_1 = -1, x_2 = -1 `$

##### (3) $` x_1 = 0, x_2 = 1 `$

##### (4) No solution

##### (solution) (4) No solution


### Question 2. What are systems of linear equations?

##### (1) a set of two or more linear equations in the same variables

##### (2) a set of two or more linear inequalities in the same variables

##### (3) a set of two or more linear equations and linear inequalities in the same variables

##### (4) a polynomial with three terms

##### (solution) (1) a set of two or more linear equations in the same variables


### Question 3. What is the first order Talyor approximation of $` f(x) = e^x `$ near $` a `$?

##### (1) 1 + $` e^x `$

##### (2) 1 + $` e^a `$
 
##### (3) $` e^a +  e^a `$

##### (4) $` e^a + e^a(x-a) `$


##### (solution) (4) $` e^a + e^a(x-a) `$


## [Week 04 - Unit 01] (Matrix Multiplication)

### Question 1. Compute the multiplication $`C = A B`$ of matrices $`A = \begin{bmatrix} 1 & 6\\ 9 & 3 \end{bmatrix}`$ and $`B = \begin{bmatrix} 0 & -1\\ -1 & 2 \end{bmatrix}`$

##### (1) $`C = \begin{bmatrix} 0 & -6\\ -9 & 6 \end{bmatrix}`$

##### (2) $`C = \begin{bmatrix} -6 & 11\\ -3 & -3 \end{bmatrix}`$

##### (3) $`C = \begin{bmatrix} -9 & -3\\ 17 & 0 \end{bmatrix}`$

##### (4) $`C = \begin{bmatrix} 1 & 5\\ 8 & 5 \end{bmatrix}`$

##### (solution) (2) $`C = \begin{bmatrix} -6 & 11\\ -3 & -3 \end{bmatrix}`$


### Question 2. Compute the third power $`A^{3}`$ of matrix $`A = \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$

##### (1) $`A^{3} = \begin{bmatrix} 0&1&0\\1&1&0\\1&0&1\end{bmatrix}`$

##### (2) $`A^{3} = \begin{bmatrix} 1&1&0\\1&1&1\\0&0&1\end{bmatrix}`$

##### (3) $`A^{3} = \begin{bmatrix} 1&0&1\\0&1&1\\1&0&1\end{bmatrix}`$

##### (4) $`A^{3} = \begin{bmatrix} 1&1&1\\1&1&1\\1&1&1\end{bmatrix}`$

##### (solution) (3) $`A^{3} = \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$ = $`\begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$ = $`\begin{bmatrix} 1&0&1\\0&1&1\\1&0&1\end{bmatrix}`$


### Question 3. Choose one _wrong_ explanation for the properties of matrix multiplication. Let A, B and C be the matrices, $`A^{T}`$ be the transposed matrix of matrix A, and $`I`$ be the Identity matrix.

##### (1) $`AI = A`$ and $`IA = A`$ 

##### (2) $`A(B+C) = A(B \times C)`$ 

##### (3) $`(AB)^{T} = B^{T}A^{T}`$ 

##### (4) $`AB = BA`$ _does not hold in general_

##### (solution) (2) $`A(B+C) = AB + AC`$ 





## [Week 04 - Unit 02] (Matrix Inverses)

### Question 1. Calculate the inverse of 2x2 matrix $` A = \begin{bmatrix} 4 & 7 \\ 2 & 6 \end{bmatrix}`$.

##### (1) $`A^{-1} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$
##### (2) $`A^{-1} = \begin{bmatrix} 0.4 & -0.7 \\ -0.1 & 0.4 \end{bmatrix}`$ 
##### (3) $`A^{-1} = \begin{bmatrix} 0.2 & -0.5 \\ -0.3 & 0.2 \end{bmatrix}`$
##### (4) $`A^{-1} = \begin{bmatrix} 0.1 & -0.3 \\ -0.2 & 0.4 \end{bmatrix}`$ 
##### (solution) (1) $`A^{-1} = \frac{1}{4\times6 - 7\times2}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \frac{1}{10}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$


### Question 2. Solve the given system of equations using the inverse of matrix.
(1). $`3x + 8y = 5`$  
(2). $`4x + 11y = 7`$
$`\newline`$(Hint: $`\begin{bmatrix}3 & 8 \\ 4 & 11 \end{bmatrix}\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}5\\7\end{bmatrix} `$)


##### (1) $`x = 1 y = 1`$
##### (2) $`x = -1 y = 1`$
##### (3) $`x = -1 y = 0`$
##### (4) $`x = 1 y = 0`$
##### (solution) (2) $`x = -1 y = 1`$
First, let $`A`$ be the coefficient matrix of the system, $`X`$ be the variable matrix, and $`B`$ be the constant matrix. Then, we can represent matrix A as $`A = \begin{bmatrix}3&8\\4&11\end{bmatrix}`$, matrix X as $`X = \begin{bmatrix}x\\y\end{bmatrix}`$ and matrix B as $`B = \begin{bmatrix}5\\7\end{bmatrix}`$.
Second, we need to calculate the inverse matrix $`A^{-1}`$ of matrix $`A`$, then we have $`A^{-1} = \begin{bmatrix}11&-8\\-4&3\end{bmatrix}`$. 
Lastly, we multiply both sides of the equation by $`A^{-1}`$. 
$`(A^{-1})AX = (A^{-1})B`$
$`\begin{bmatrix}11&-8\\-4&3\end{bmatrix}\begin{bmatrix}3&8\\4&11\end{bmatrix}\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}11&-8\\-4&3\end{bmatrix}\begin{bmatrix}5\\7\end{bmatrix}`$
$`\begin{bmatrix}1&0\\0&1\end{bmatrix}\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}11(5)+(-8)7\\-4(5)+3(7)\end{bmatrix}`$
$`\begin{bmatrix}x\\y\end{bmatrix} = \begin{bmatrix}-1\\1\end{bmatrix}`$
Therefore, $`x = -1 y = 1`$

### Question 3. Choose one wrong explanation for the properties of inverse of matrix. Let A be the matrix, $`A^{T}`$ be the transposed matrix of matrix A, and $`I`$ be the Identity matrix.
##### (1) If $`A`$ is a square with orthonormal columns, we have $`A^{-1} = A{T}`$
##### (2) If $`A`$ is a square invertible matrix , $`(A^{T})^{-1} = (A^{-1})^{T}`$
##### (3) If $`A`$ is invertible and $`B`$ is not invertible, $`(AB)^{-1} = B^{-1}A^{-1}`$
##### (4) If $`A`$ is a square invertible matrix and k $`k`$ is a positive integer, $`(A^{k})^{-1} = (A^{-1})^{k}`$
##### (solution) (3) Both A and B matrices should be square invertible matrices (of the same size) to have the property $`(AB)^{-1} = B^{-1}A^{-1}`$.



## [Week 05 - Unit 01] (Least Squares)

### Question 1. Given a system of linear equations $`Ax=b`$ where $`A`$ is a $`m \times n`$ matrix, and $`x`$ is $`n`$-vector and $`b`$ is an $`m`$-vector, find an objective function of the least square problem.  

##### (1) $`\lVert Ax-b \rVert`$

##### (2) $`\lVert Ax-b \rVert ^2`$

##### (3) $`\lVert Ax \rVert`$

##### (4) $`\lVert Ax \rVert ^2`$

##### (solution) (2) $`\lVert Ax-b \rVert ^2`$

### Question 2. Let $`Ax=b`$ be a system of linear equations where $`A`$ is a $`m \times n`$ matrix, and $`x`$ is $`n`$-vector and $`b`$ is an $`m`$-vector. Among the followings, which statement should be assumed when the least squares solution of $`Ax=b`$ is to be $`x=A^{-1}b`$?

##### (1) $`A`$ has linearly dependent rows.

##### (2) $`A`$ has linearly independent rows.

##### (3) $`A`$ has linearly dependent columns.

##### (4) $`A`$ has linearly independent columns.

##### (solution) (4) $`A`$ has linearly independent columns.


## [Week 05 - Unit 02] (Least Squares Data Fitting)

### Question 1. Let $`x`$ and $`y`$ be the independent variable and outcome, repectively. $`f: \mathbf{R}^{n} \to \mathbf{R}`$ gives the relation between $`x`$ and $`y`$, $`f_i: \mathbf{R}^{n} \to \mathbf{R}`$ are basis functions and $`\hat f:\mathbf{R}^{n} \to \mathbf{R}`$ is a model to approximate the real relation $`f`$.  $`x^{(i)}`$, $`y^{(i)}`$ is $`i`$-th data pair and $`x_{j}^{(i)}`$ is the $`j`$-th component (value) of $`i`$-th data point. $`N \times p`$ matrix $`A`$ is defined by $`A_{ij}=\hat f_{j}(x^{(i)})`$ where $`i=1,...,N`$ and $`j=1,...,p`$. $`\theta_i`$ are model parameters. $`y^{d}`$ denotes vector of the desired outcomes and $`\hat y^{d}=A\theta`$ is vector of predictions. Among the followings, whose value we want to get by least square data fitting?

##### (1) $`\theta`$

##### (2) $`y^d`$

##### (3) $`A`$

##### (4) $`f`$

##### (solution) (1) $`\theta`$

### Question 2. Let $`A`$, $`\theta`$, $`y`$, and $`y^{d}`$ follow the same definition as in Question 1 and assume that column vectors of $`A`$ are linearly independent. Which of the followings is the model parameter values $`\hat \theta`$ that minimize the norm of the prediction error $`\lVert A\theta - y^d \rVert ^2`$ by least squares data fitting?

##### (1) $`\hat \theta = A^{T}(A^{T}A)^{-1}y`$

##### (2) $`\hat \theta = y(A^{T}A)^{-1}A^{T}`$

##### (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$

##### (4) $`\hat \theta = A^{T}(AA^{T})^{-1}y`$

##### (solution) (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$
