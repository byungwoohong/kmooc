# [Week 07] (Logistic Regression)

## Question 1. The code block below is to compute the logistic regression loss. Choose the best option to complete the code block, filling in the blank. 

A module ```compute_loss``` is to compute the logistic regression loss which is defined as $` L(w)=-\frac{1}{n} \sum_{i=1}^n \ \Big(  y_i \log(\sigma(w\cdot x_i)) + (1-y_i)\log(1-\sigma(w\cdot x_i)) \Big) `$ where $` \sigma(\eta) = \frac{1}{1+e^{-\eta}} `$, $` x `$ is the input data, $` y `$ is the ground truth, $` n `$ is the number of data and $` w `$ is the trainable model parameter (weight).

```python
# The length of x is assumed to be the same as the length of y.

import numpy

def compute_loss(x, w, y): # x and y are numpy arrays. w is an unknown scalar variable.
    n           =   y.shape[0]
    y_predict   =   1 / (1 +numpy.exp(-w*x))
    loss        = ### fill in the black ###

    return loss
```

#### (1) 
```python
    loss = 1/n* ( y.T.dot(np.log(y_predict)) + (1-y).T.dot(np.log(1-y_predict)) )
```

#### (2)
```python
    loss = -1/n* ( y.T.dot(np.log(y_predict)) + (1-y).T.dot(np.log(1-y_predict)) )
```

#### (3)
```python
    loss = -1/n* ( y_predict.T.dot(np.log(y)) + (1-y_predict).T.dot(np.log(1-y)) )
```

#### (4)
```python
    loss = 1/n* ( y_predict.T.dot(np.log(y)) + (1-y_predict).T.dot(np.log(1-y)) )
```

#### (solution) (2)
```python
    loss = -1/n* ( y.T.dot(np.log(y_predict)) + (1-y).T.dot(np.log(1-y_predict)) )
```

## Question 2. The code block below is to compute the gradient of the logistic regression loss. Choose the best option to complete the code block, filling in the blank.  

A module ```compute_gradient``` is to compute the logistic regression loss function which is defined as $` L(w)=-\frac{1}{n} \sum_{i=1}^n \ \Big(  y_i \log(\sigma(w\cdot x_i)) + (1-y_i)\log(1-\sigma(w\cdot x_i)) \Big) `$ where $` \sigma(\eta) = \frac{1}{1+e^{-\eta}} `$, $` x `$ is the input data, $` y `$ is the ground truth, $` n `$ is the number of data and $` w `$ is the trainable parameter (weight).

```python
# The length of x is assumed to be the same as the length of y.

import numpy

def compute_gradient(x, w, y): # x and y are numpy arrays. w is an unknown variable.
    n           =   y.shape[0]
    y_predict   =   1 / (1 +numpy.exp(-w*x))
    grad        = ### fill in the black ###

    return grad
```

#### (1) 
```python
    grad = -2/n* y.T.dot(y_predict-y)
```

#### (2)
```python
    grad = 2/n* y.T.dot(y_predict-y)
```

#### (3)
```python
    grad = -2/n* x.T.dot(y_predict-y)
```

#### (4)
```python
    grad = 2/n* x.T.dot(y_predict-y)
```

#### (solution) (4)
```python
    grad = 2/n* x.T.dot(y_predict-y)
```
