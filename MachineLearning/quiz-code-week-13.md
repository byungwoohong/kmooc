# [Week 13] (Neural Network)

Let $`x\in \mathbf{R}^{m}`$ be observed data and $`W`$ be a model parameter (weight) matrix of one fully connected layer which outputs $`k`$ number of activations, skipping bias components. Say that a sigmoid function $`\delta`$ is adopted as an activation function for the layer. The following Python code snippet defines a fully connected layer class and a forward process of the layer with some blanks to be completed, setting $`m=16`$ and $`k=8`$.

```python
import numpy as np

class FullyConnectedLayer(object):
    def __init__(self, height, width):
        self.height = height
        self.width = width
        self.W = np.zeros((self.height, self.width))

    def initialize_random_weights(self, mu, sigma):
        self.W = np.random.normal(mu, sigma, size=(self.height, self.width))
        return

    def forward_pass(self, X):
        return #-----blank-----#

def sigmoid(x):
    return 1 / (1 + np.exp(-x))

if __name__=="__main__":
    m = 16
    k = 8
    X = np.random.randint(0, 10, m)
    mu, sigma = 0, 0.1
    #-----blank-----#
    #-----blank-----#
    #-----blank-----#
```

## Unit13-1. Given the code snippet above, which one best compute the matrix vector multiplication of the fully connected layer in the forward pass module ```def forward_pass```, filling in the blank? 


#### (1) 
```python
sigmoid(self.W*X)
```

#### (2)
```python
sigmoid(X*self.W)
```

#### (3)
```python
self.W*sigmoid(X)
```

#### (4)
```python
sigmoid(X)*self.W
```

#### (solution) (1)
```python
sigmoid(self.W*X)
```

## Unit13-2. Given the code snippet above, which one describes the most appropriate sequence of processes to get outputs from the fully connected layer, filling in the three blanks in the ```__main__``` code?


#### (1) 
```python
fc_layer = FullyConnectedLayer(k, m)
y = fc_layer.forward_pass(X)
fc_layer.initialize_random_weights(mu, sigma)
```

#### (2)
```python
fc_layer.initialize_random_weights(mu, sigma)
fc_layer = FullyConnectedLayer(k, m)
y = fc_layer.forward_pass(X)
```

#### (3)
```python
fc_layer = FullyConnectedLayer(k, m)
fc_layer.initialize_random_weights(mu, sigma)
y = fc_layer.forward_pass(X)
```

#### (4)
```python
y = fc_layer.forward_pass(X)
fc_layer.initialize_random_weights(mu, sigma)
fc_layer = FullyConnectedLayer(k, m)
```

#### (solution) (3)
```python
fc_layer = FullyConnectedLayer(k, m)
fc_layer.initialize_random_weights(mu, sigma)
y = fc_layer.forward_pass(X)
```