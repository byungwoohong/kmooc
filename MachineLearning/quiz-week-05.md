## Week 05

### Question 1. Given a system of linear equations $`Ax=b`$ where $`A`$ is a $`m \times n`$ matrix, and $`x`$ is a $`n`$-vector and $`b`$ is an $`m`$-vector, find an objective function of the least square problem.  

#### (1) $`\lVert Ax-b \rVert`$

#### (2) $`\lVert Ax-b \rVert ^2`$

#### (3) $`\lVert Ax \rVert`$

#### (4) $`\lVert Ax \rVert ^2`$

#### (solution) (2) $`\lVert Ax-b \rVert ^2`$


### Question 2. Let $`Ax=b`$ be a system of linear equations where $`A`$ is a $`m \times n`$ matrix, and $`x`$ is $`n`$-vector and $`b`$ is an $`m`$-vector. Among the followings, which statement should be assumed when the least squares solution of $`Ax=b`$ is to be $`x=A^{-1}b`$?

#### (1) $`A`$ has linearly dependent rows.

#### (2) $`A`$ has linearly independent rows.

#### (3) $`A`$ has linearly dependent columns.

#### (4) $`A`$ has linearly independent columns.

#### (solution) (4) $`A`$ has linearly independent columns.


### Question 3. Which of the followings is correct for the $`L`$-2 norm square?

#### (1) $`\lVert x \rVert^2 = x^T x`$

#### (2) $`\lVert x \rVert^2 = x x^T`$

#### (3) $`\lVert x \rVert^2 = x x`$

#### (4) $`\lVert x \rVert^2 = x^T x^T`$

#### (solution) (1) $`\lVert x \rVert^2 = x^T x`$


### Question 4. Let $`A`$ be a matrix and its columns are assumed to be linearly independent. Let $`\theta`$ be the unknown model parameters and $`y`$ be a given vector. Which of the followings is a solution $`\hat{\theta}`$ for the least square problem $`\lVert A\theta - y \rVert ^2`$?

#### (1) $`\hat \theta = A^{T}(A^{T}A)^{-1}y`$

#### (2) $`\hat \theta = y(A^{T}A)^{-1}A^{T}`$

#### (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$

#### (4) $`\hat \theta = A^{T}(AA^{T})^{-1}y`$

#### (solution) (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$
