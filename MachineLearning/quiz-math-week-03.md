## [Week 03 - Unit 01] (Matrices)

### Question 1. Compute the multiplication $`C = A B`$ of matrices $`A = \begin{bmatrix} 1 & 2\\ 3 & 4 \end{bmatrix}`$ and $`B = \begin{bmatrix} 4 & 3\\ 2 & 1 \end{bmatrix}`$

##### (1) $`C = \begin{bmatrix} 8 & 5\\ 20 & 13 \end{bmatrix}`$

##### (2) $`C = \begin{bmatrix} 4 & 6\\ 6 & 4 \end{bmatrix}`$

##### (3) $`C = \begin{bmatrix} 2 & 12\\ 12 & 2 \end{bmatrix}`$

##### (4) $`C = \begin{bmatrix} 5 & 5\\ 5 & 5 \end{bmatrix}`$

##### (solution) (1) $`C = \begin{bmatrix} 8 & 5\\ 20 & 13 \end{bmatrix}`$


### Question 2. Suppose the block matrix $` \begin{bmatrix} A & I\\ I & C \end{bmatrix}`$ make senses, where size of $`A`$ is $`p \times q `$. What are the dimension of $`C`$?

##### (1) $` q \times p `$

##### (2) $` p \times q `$

##### (3) $` (p+p) \times (q+q) `$

##### (4) $` (p+q) \times (p+q) `$

##### (solution) (2) $` p \times q `$


### Question 3. Compute the $`\|A\|`$ when $` A = \begin{bmatrix} 2 & 1 \\ 2 & 3 \end{bmatrix} `$.

##### (1) $` \sqrt{8} `$

##### (2) $` \sqrt{18} `$

##### (3) $` 8 `$

##### (4) $` 18 `$

##### (solution) (2) $` \sqrt{18} `$



## [Week 03 - Unit 02] (Linear Equations)

### Question 1. What is the solution for the set of linear equations $` x_1 + x_2 = 1, x_1 = -1, x_1 - x_2 = 0 `$

##### (1) $` x_1 = -1, x_2 = 2 `$

##### (2) $` x_1 = -1, x_2 = -1 `$

##### (3) $` x_1 = 0, x_2 = 1 `$

##### (4) No solution

##### (solution) (4) No solution


### Question 2. What are systems of linear equations?

##### (1) a set of two or more linear equations in the same variables

##### (2) a set of two or more linear inequalities in the same variables

##### (3) a set of two or more linear equations and linear inequalities in the same variables

##### (4) a polynomial with three terms

##### (solution) (1) a set of two or more linear equations in the same variables


### Question 3. What is the first order Talyor approximation of $` f(x) = e^x `$ near $` a `$?

##### (1) 1 + $` e^x `$

##### (2) 1 + $` e^a `$
 
##### (3) $` e^a +  e^a `$

##### (4) $` e^a + e^a(x-a) `$


##### (solution) (4) $` e^a + e^a(x-a) `$
