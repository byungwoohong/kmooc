# Week 10

### Question 1. Let $`\ell = (\ell_1, \ell_2, \cdots, \ell_n), \quad \forall \ell_i \in \{ 0, 1 \}`$ be a list of the ground truth for the classification problem for a given data set $`\{ x_i \}_{i=1}^n`$. Let $`p = (p_1, p_2, \cdots, p_n), \quad \forall p_i \in \{ 0, 1 \}`$ be the associated prediction for the data set $`\{ x_i \}_{i=1}^n`$. Which of the followings is the most appropriate to fill the blank in the code block for computing the accuracy of the prediction.  

```python
import numpy as np

def accuracy(l, p): 
 
    n           =   l.shape[0]
    match       =   np.sum(l == p)
    accuracy    = ## fill in the blank ##

    return accuracy
```

#### (1) 
```python
    accuracy = n - match
```

#### (2)
```python
    accuracy = ( n - match ) / n
```

#### (3)
```python
    accuracy = match * n
```

#### (4)
```python
    accuracy = match / n

```

#### (solution) (4)
```python
    accuracy = match / n
```

### Question 2. Let us denote by $`TP, TN, FP, FN`$ the true positive, true negative, false positive and false negative, respectively. Which of the followings is the most appropriate for the definition of `precision`.

#### (1) $`\frac{TP}{TP + FP}`$

#### (2) $`\frac{FP}{TP + FN}`$

#### (3) $`\frac{FP}{TP + TN}`$

#### (4) $`\frac{FN}{FP + FN}`$

#### (solution) (1)

### Question 3. Let us denote by $`TP, TN, FP, FN`$ the true positive, true negative, false positive and false negative, respectively. Which of the followings is the most appropriate for the definition of `recall`.

#### (1) $`\frac{TP}{TP + FP}`$

#### (2) $`\frac{TP}{TP + FN}`$

#### (3) $`\frac{TP}{TP + TN}`$

#### (4) $`\frac{TP}{FP + FN}`$

#### (solution) (2)