# [Week 11] (Unsupervised Learning)

This is a part of the the class **Kmeans** which is designed to cluster 2D data by k-means clustering algorithm. $`k`$ denotes to the number of class.

We use the Euclidian distance $` D(p, q) = \sqrt{(p_{1}-q_{1})^{2}+(p_{2}-q_{2})^{2}} `$ to calculate distance between data and centroid. 

Choose the best option for filling in the blanks.


```python
#k is an integer number of clustering class
#The dictionary centroids are for saving the centroid points
#The dictionary classifications are for saving data for each class

import numpy as np

class Kmeans:
    def __init__(self, k=2, max_iter=300):
        self.k = k
        self.max_iter = max_iter
        
    def run(self, data):
        
        self.centroids = {}
        
        # initialize k centroids
        for i in range(self.k):
            self.centroids[i] = data[i]
    
        # iteration
        for i in range(self.max_iter):
        
            # save data points in classifications by class
            self.classifications = {}

            for i in range(self.k):
                self.classifications[i] = []
            
            # for each data points
            for point in data:
                distances = []
                
                # calculate distance for centroid
                for i in range(self.k):
                    centroid = self.centroids[i]
                    distance = ## fill in the blank ##
                    distances.append(distance)
                
                # assigning class labels
                classification = ## fill in the blank ##
                self.classifications[classification].append(featureset)
        
        
        # omitted below
```


## Unit10-1. Choose the best option for distance of centroid in the following code blocks to achieve the specified functionality.



#### (1) 
```python
    distance = point - centroid
```

#### (2)
```python
    distance = np.sum(np.dot(a, centroid))
```

#### (3)
```python
    distance = np.sum(point - centroid)
```

#### (4)
```python
    distance = np.sqrt(np.sum((point - centroid)**2))

```

#### (solution) (4)
```python
    distance = np.sqrt(np.sum((point - centroid)**2))
```


## Unit09-2. Choose the best option for assigning class labels in the following code blocks to achieve the specified functionality.

#### (1) 
```python
    classification = np.argmin(distances)
```

#### (2)
```python
    classification = np.sum(distances)
```

#### (3)
```python
    classification = np.sum(np.min(distances))
```

#### (4)
```python
    classification = np.min(distances)

```

#### (solution) (1)
```python
    classification = np.argmin(distances)
```