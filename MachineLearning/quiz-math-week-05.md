## [Week 05 - Unit 01] (Least Squares)

### Question 1. Given a system of linear equations $`Ax=b`$ where $`A`$ is a $`m \times n`$ matrix, and $`x`$ is $`n`$-vector and $`b`$ is an $`m`$-vector, find an objective function of the least square problem.  

##### (1) $`\lVert Ax-b \rVert`$

##### (2) $`\lVert Ax-b \rVert ^2`$

##### (3) $`\lVert Ax \rVert`$

##### (4) $`\lVert Ax \rVert ^2`$

##### (solution) (2) $`\lVert Ax-b \rVert ^2`$

### Question 2. Let $`Ax=b`$ be a system of linear equations where $`A`$ is a $`m \times n`$ matrix, and $`x`$ is $`n`$-vector and $`b`$ is an $`m`$-vector. Among the followings, which statement should be assumed when the least squares solution of $`Ax=b`$ is to be $`x=A^{-1}b`$?

##### (1) $`A`$ has linearly dependent rows.

##### (2) $`A`$ has linearly independent rows.

##### (3) $`A`$ has linearly dependent columns.

##### (4) $`A`$ has linearly independent columns.

##### (solution) (4) $`A`$ has linearly independent columns.


## [Week 05 - Unit 02] (Least Squares Data Fitting)

### Question 1. Let $`x`$ and $`y`$ be the independent variable and outcome, respectively. $`f: \mathbf{R}^{n} \to \mathbf{R}`$ gives the relation between $`x`$ and $`y`$, $`f_i: \mathbf{R}^{n} \to \mathbf{R}`$ are basis functions and $`\hat f:\mathbf{R}^{n} \to \mathbf{R}`$ is a model to approximate the real relation $`f`$.  $`x^{(i)}`$, $`y^{(i)}`$ is $`i`$-th data pair and $`x_{j}^{(i)}`$ is the $`j`$-th component (value) of $`i`$-th data point. $`N \times p`$ matrix $`A`$ is defined by $`A_{ij}=\hat f_{j}(x^{(i)})`$ where $`i=1,...,N`$ and $`j=1,...,p`$. $`\theta_i`$ are model parameters. $`y^{d}`$ denotes vector of the desired outcomes and $`\hat y^{d}=A\theta`$ is vector of predictions. Among the followings, whose value we want to get by least square data fitting?

##### (1) $`\theta`$

##### (2) $`y^d`$

##### (3) $`A`$

##### (4) $`f`$

##### (solution) (1) $`\theta`$

### Question 2. Let $`A`$, $`\theta`$, $`y`$, and $`y^{d}`$ follow the same definition as in Question 1 and assume that column vectors of $`A`$ are linearly independent. Which of the followings is the model parameter values $`\hat \theta`$ that minimize the norm of the prediction error $`\lVert A\theta - y^d \rVert ^2`$ by least squares data fitting?

##### (1) $`\hat \theta = A^{T}(A^{T}A)^{-1}y`$

##### (2) $`\hat \theta = y(A^{T}A)^{-1}A^{T}`$

##### (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$

##### (4) $`\hat \theta = A^{T}(AA^{T})^{-1}y`$

##### (solution) (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$
