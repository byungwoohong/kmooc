# [Week 06] (Linear Regression)

## Question 1. Choose the best option for the blank in the following code block to complete a module ```def compute_accuracy```.  

Let $`l`$ be a list of elements representing the ground truth and $`h`$ be a list of elements representing the prediction. The module ```compute_accuracy``` is designed to compute the accuracy of the prediction.

```python
# The length of h is assumed to be the same as the length of l.

import numpy

def compute_accuracy(l, h): # l and h are numpy arrays

    n           = l.shape[0]
    match       = numpy.sum(l == h)
    accuracy    = ### fill in the black ###

    return accuracy
```

#### (1) 
```python
    accuracy = n - match
```

#### (2)
```python
    accuracy = ( n - match ) / n
```

#### (3)
```python
    accuracy = match * n
```

#### (4)
```python
    accuracy = match / n
```

#### (solution) (4)
```python
    accuracy = match / n
```


## Question 2. The code block below is to compute the linear regression loss. Choose the best option to complete the code block, filling in the blank.  

A module ```compute_loss``` is to compute the linear regression loss function which is defined as $` L(w)=\frac{1}{n} \sum_{i=1}^n \ \Big( w\cdot x_i – y_i \Big)^2 `$ where $` x `$ is the input data, $` y `$ is the ground truth, $` n `$ is the number of data and $` w `$ is the trainable model parameter (weight).

```python
# The length of x is assumed to be the same as the length of y and w is scalar.

import numpy

def compute_loss(x, w, y): # x and y are numpy arrays. w is an unknown scalar variable.
    n           =   y.shape[0]
    y_predict   =   x * w
    loss        = ### fill in the black ###

    return loss
```

#### (1) 
```python
    loss = 1/n * (y_predict - y).T.dot(y_predict - y)
```

#### (2)
```python
    loss = 1/n * (y_predict + y).T.dot(y_predict + y)
```

#### (3)
```python
    loss = 1/n * (y_predict - y).T.dot(y_predict + y)
```

#### (4)
```python
    loss = 1/n * (y_predict + y).T.dot(y_predict - y)
```

#### (solution) (1)
```python
    loss = 1/n * (y_predict - y).T.dot(y_predict - y)
```

## Question 3. The code block below is to compute the gradient of the linear regression loss. Choose the best option to complete the code block, filling in the blank.

The linear regression loss function is defined as $` L(w)=\frac{1}{n} \sum_{i=1}^n \ \Big( w\cdot x_i – y_i \Big)^2 `$. A module ```compute_gradient``` computes the gradient of linear regression loss $`L(w)`$ where $` x `$ is the input data, $` y `$ is the ground truth and $` n `$ is the number of data and $` w `$ is the trainable model parameter (weight).

```python
# The length of x and y are assumed to be the same as the length of z.

import numpy

def compute_gradient(x, w, y): # x and y are numpy arrays. w is an unknown scalar variable.
    n           =   y.shape[0]
    y_predict   =   x * w
    grad        = ### fill in the black ###

    return loss
```

#### (1) 
```python
    grad = 2/n * x.T.dot(x-y)
```

#### (2)
```python
    grad = 2/n * y.T.dot(y_predict-y)
```

#### (3)
```python
    grad = 2/n * x.T.dot(y_predict-y)
```

#### (4)
```python
    grad = 2/n * y_predict.T.dot(y_predict-y)
```

#### (solution) (3)
```python
    grad = 2/n * x.T.dot(y_predict-y)
```
