# [Week 10] (Developing Machine Learning Project)

## Question1. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.  

Let $`ytrue`$ be a list of elements representing the ground truth and $`ypred`$ be a list of elements representing the prediction. The function is designed to compute the accuracy of the prediction.

```python
# The function "accuracy" is designed to compute the accuracy of the prediction for classification task.
 # Each element of the ground truth (y_true) is either 0 or 1.
 # Each element of the predicition (y_pred) is either 0 or 1.

import numpy as np

def accuracy(y_true, y_pred): # y_true and y_pred numpy arrays
 
    n           =   y_true.shape[0]
    match       =   np.sum(y_true == y_pred)
    accuracy    =   ## fill in the blank ##

    return accuracy
```

#### (1) 
```python
    accuracy = n - match
```

#### (2)
```python
    accuracy = ( n - match ) / n
```

#### (3)
```python
    accuracy = match * n
```

#### (4)
```python
    accuracy = match / n

```

#### (solution) (4)
```python
    accuracy = match / n
```

## Question2. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.  

Let $`ytrue`$ be a list of elements representing the ground truth and $`y pred`$ be a list of elements representing the prediction. The function is designed to compute the **f1-score** of the prediction. Find an appropriate f1-score expression using precision and recall.

```python
# F1 score measurement for binary classification
 # Each element of the ground truth (y_true) is either 0 or 1.
 # Each element of the predicition (y_pred) is either 0 or 1.

import numpy as np

def f1_score(y_true, y_pred): # y_true and y_pred numpy arrays
    tp = np.sum((y_true == 1) & (y_pred == 1)) #true positive
    fn = np.sum((y_true == 1) & (y_pred == 0)) #false negative
    fp = np.sum((y_true == 0) & (y_pred == 1)) #false positive

    precision   =   tp/(tp+fp)                
    recall      =   tp/(tp+fn)

    f1score     =   ## fill in the blank ##

    return f1score

```

#### (1) 
```python
    f1score = 1.5*(precision*recall)/(precision+recall)  
```

#### (2)
```python
    f1score = 2*(precision+recall)/(precision*recall)  
```

#### (3)
```python
    f1score = 2*(precision*recall)/(precision+recall)  
```

#### (4)
```python
    f1score = 1.5*(precision+recall)/(precision*recall)  
```

#### Solution (3)
```python
    f1score = 2*(precision*recall)/(precision+recall)  
```

