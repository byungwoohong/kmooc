# Week 13

### Question 1. Consider a fully connect layer without a bias in a neural network. When the fully connect layer has $`m`$ inputs and $`n`$ outputs, determine the number of weights. 

#### (1) $`m + n`$

#### (2) $`m^n`$

#### (3) $`m  n`$

#### (4) $`n^m`$

#### (solution) (3)

### Question 2. Let $`x`$ and $`y`$ be vectors of input of size $`m`$ and output of size $`n`$ for the fully connected layer in a neural network. Let $W$ be a matrix representing weights that consist of the fully connected layer leading to the forward-propagation $`y = W x`$. Determine the size of weight matrix $`W`$. 

#### (1) $`m \times n`$

#### (2) $`n \times m`$

#### (3) $`1 \times m n`$

#### (4) $`m n \times 1`$

#### (solution) (2)