# [Week 12] (Neural Network)

## Question1. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.  

Let x be a numpy array of elements. The function is designed to compute **sigmoid** activation function. Find the right sigmoid expression among these four options.

```python
# variable x : numpy array 
# Sigmoid function is based on the sigmoid equation

import numpy as np

def sigmoid(x):
    sigmoid_x   = ## fill in the blank ##
    return sigmoid_x

```

#### (1) 
```python
    sigmoid_x = 1 / (1 + np.exp(x))
```

#### (2)
```python
    sigmoid_x = 1 / (1 + np.exp(-x))  
```

#### (3)
```python
    sigmoid_x = 1 / (x + np.exp(-x))
```

#### (4)
```python
    sigmoid_x = 1 / (x + np.exp(x))
```

#### Solution (2)
```python
    sigmoid_x = 1 / (1 + np.exp(-x))  
```

## Question2. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.  

Let x be a numpy array of elements. Compute the **derivative of the sigmoid** activation function and find the expression among these four options. 

```python
# variable "x" : input array 
# variable "result" : the result of derivative of the sigmoid function.

import numpy as np

def derivative_of_sigmoid(x):
    sigmoid     = 1 / (1 + np.exp(-x))
    result      = ## fill in the blank ##
    return result
```

#### (1) 
```python
    result = sigmoid * (1-sigmoid)  
```

#### (2)
```python
    result = sigmoid * (1+sigmoid)
```

#### (3)
```python
    result = sigmoid / (1-sigmoid)
```

#### (4)
```python
    result = sigmoid / (1*sigmoid)
```

#### Solution (1)
```python
    result = sigmoid * (1-sigmoid)  
```
