# Week 12


### Question 1. Which of the followings is the correct definition of `sigmoid` function. Note that the associated parameters for the steepness and the shift are not considered in the definition.

#### (1) $`\textrm{sigmoid}(x) = \frac{1}{1 + e^{-x}}`$

#### (2) $`\textrm{sigmoid}(x) = \frac{1}{x + e^{-x}}`$

#### (3) $`\textrm{sigmoid}(x) = \frac{x}{1 + e^{-x}}`$

#### (4) $`\textrm{sigmoid}(x) = \frac{x}{x + e^{-x}}`$

#### Solution (1)

### Question 2. Which of the followings is the correct derivation for the derivative of `sigmoid` function when `sigmoid` function $`\sigma(x)`$ is given.

#### (1) $`\frac{d \sigma(x)}{d x} = \sigma(x) \cdot (x + \sigma(x))`$

#### (2) $`\frac{d \sigma(x)}{d x} = \sigma(x) \cdot (x - \sigma(x))`$

#### (3) $`\frac{d \sigma(x)}{d x} = \sigma(x) \cdot (1 + \sigma(x))`$

#### (4) $`\frac{d \sigma(x)}{d x} = \sigma(x) \cdot (1 - \sigma(x))`$

#### Solution (4)