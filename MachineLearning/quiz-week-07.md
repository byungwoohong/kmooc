# Week 07

### Question 1. Let $`p`$ and $`q`$ be distributions over a given set $`X`$. Choose the correct definition of the cross-entropy of the distribution $`q`$ relative to the distribution $`p`$. 

#### (1) $`H(p, q) = \sum_{x \in X} p(x) \log( q(x) )`$

#### (2) $`H(p, q) = - \sum_{x \in X} p(x) \log( q(x) )`$

#### (3) $`H(p, q) = \sum_{x \in X} \log( p(x) \, q(x) )`$

#### (4) $`H(p, q) = - \sum_{x \in X} p(x) \, q(x)`$

#### (solution) (2)

### Question 2. In a binary classification problem with the cross-entropy loss, let $`\ell \in \{ 0, 1 \}`$ be a variable for the class label of a data and $`h \in [0, 1]`$ be its predictive probability. Which of the followings is the most appropriate loss when the class label $`\ell = 1`$.

#### (1) $`\log h`$

#### (2) $`h`$

#### (3) $`-\log h`$

#### (4) $`-h`$

#### (solution) (3)