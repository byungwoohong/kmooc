# Week 11

### Question 1. Let $`p \in \mathbb{R}^2`$ and $`q \in \mathbb{R}^2`$ be points in the 2-dimensional cartesian coordinate. Which of the followings is the most appropriate for the Euclidean distance between the two points $`p`$ and $`q`$. Note that $`\| \cdot \|_2`$ denotes the $`L`$-2 norm.

#### (1) $`p + q`$

#### (2) $`p - q`$

#### (3) $`\| p \|_2 - \|q\|_2`$

#### (4) $`\| p - q \|_2`$

#### (solution) (4)

### Question 2. Let $`p_i = (x_i, y_i) \in \mathbb{R}^2`$ be a point in the 2-dimensional cartesian coordinate. Which of the followings is the most appropriate for the computation of centroid $`(\mu_x, \mu_y)`$of a given set of points $`\{ p_i \}_{i=1}^n`$.

#### (1) $`\mu_x = \frac{1}{n} \sum_{i=1}^{n} (x_i + y_i) `$ and $`\mu_y = \frac{1}{n} \sum_{i=1}^{n} (x_i + y_i)`$

#### (2) $`\mu_x = \frac{1}{n} \sum_{i=1}^{n} x_i `$ and $`\mu_y = \frac{1}{n} \sum_{i=1}^{n} y_i `$

#### (3) $`\mu_x = \sum_{i=1}^{n} x_i y_i `$ and $`\mu_y = \sum_{i=1}^{n} x_i y_i`$

#### (4) $`\mu_x = \sum_{i=1}^{n} x_i^2 `$ and $`\mu_y = \sum_{i=1}^{n} y_i^2`$

#### (solution) (2)
