## Week 03

### Question 1. Let $` \begin{bmatrix} A & I\\ I & C \end{bmatrix}`$ be a block matrix where the size of $`A`$ is $`p \times q `$. Determine the dimension of matrix $`C`$.

#### (1) $` q \times p `$

#### (2) $` p \times q `$

#### (3) $` (p+p) \times (q+q) `$

#### (4) $` (p+q) \times (p+q) `$

#### (solution) (2) $` p \times q `$


### Question 2. Compute the Frobenius norm $`\|A\|_F`$ of a matrix $`A = \begin{bmatrix} 2 & 1 \\ 2 & 3 \end{bmatrix}`$. The Frobenius norm of a matrix $`A`$ is defined by $`\left( \sum_{i} \sum_{j} | a_{ij} |^2 \right)^{\frac{1}{2}}`$ for all $`i`$ and $`j`$ where $`a_{ij}`$ denotes an element of $`A`$ at row $`i`$ and column $`j`$.

#### (1) $` \sqrt{8} `$

#### (2) $` \sqrt{18} `$

#### (3) $` 8 `$

#### (4) $` 18 `$

#### (solution) (2) $` \sqrt{18} `$


### Question 3. What is the solution for the set of linear equations $` x_1 + x_2 = 1, x_1 = -1, x_1 - x_2 = 0 `$

#### (1) $` x_1 = -1, x_2 = 2 `$

#### (2) $` x_1 = -1, x_2 = -1 `$

#### (3) $` x_1 = 0, x_2 = 1 `$

#### (4) No solution

#### (solution) (4) No solution


### Question 4. What is the first order Talyor approximation of $` f(x) = e^x `$ at $` a `$?

#### (1) 1 + $` e^x `$

#### (2) 1 + $` e^a `$
 
#### (3) $` e^a +  e^a `$

#### (4) $` e^a + e^a(x-a) `$


#### (solution) (4) $` e^a + e^a(x-a) `$
