## Week 04

### Question 1. Compute the matrix multiplication $`C = A B`$ of matrices $`A = \begin{bmatrix} 1 & 6\\ 9 & 3 \end{bmatrix}`$ and $`B = \begin{bmatrix} 0 & -1\\ -1 & 2 \end{bmatrix}`$

#### (1) $`C = \begin{bmatrix} 0 & -6\\ -9 & 6 \end{bmatrix}`$

#### (2) $`C = \begin{bmatrix} -6 & 11\\ -3 & -3 \end{bmatrix}`$

#### (3) $`C = \begin{bmatrix} -9 & -3\\ 17 & 0 \end{bmatrix}`$

#### (4) $`C = \begin{bmatrix} 1 & 5\\ 8 & 5 \end{bmatrix}`$

#### (solution) (2) $`C = \begin{bmatrix} -6 & 11\\ -3 & -3 \end{bmatrix}`$


### Question 2. Compute the third power $`A^{3}`$ of matrix $`A = \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$

#### (1) $`A^{3} = \begin{bmatrix} 0&1&0\\1&1&0\\1&0&1\end{bmatrix}`$

#### (2) $`A^{3} = \begin{bmatrix} 1&1&0\\1&1&1\\0&0&1\end{bmatrix}`$

#### (3) $`A^{3} = \begin{bmatrix} 1&0&1\\0&1&1\\1&0&1\end{bmatrix}`$

#### (4) $`A^{3} = \begin{bmatrix} 1&1&1\\1&1&1\\1&1&1\end{bmatrix}`$

#### (solution) (3) $`A^{3} = \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$ = $`\begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix} \times \begin{bmatrix} 1 & 0 & 1\\0&1&1\\1&0&1 \end{bmatrix}`$ = $`\begin{bmatrix} 1&0&1\\0&1&1\\1&0&1\end{bmatrix}`$


### Question 3. Choose all the _wrong_ explanations related to the properties of matrix multiplication. Let A, B and C be the matrices, $`A^{T}`$ be the transposed matrix of matrix A, and $`I`$ be the Identity matrix.

#### (1) $`A I = A`$ and $`I A = A`$ 

#### (2) $`A^T (B + C) = A B^T + A C^T`$ 

#### (3) $`(A B)^{T} = B^{T} A^{T}`$ 

#### (4) $`A B = B^T A^T`$

#### (solution) (2) and (4) 


### Question 4. Compute the inverse of 2x2 matrix $` A = \begin{bmatrix} 4 & 7 \\ 2 & 6 \end{bmatrix}`$.

#### (1) $`A^{-1} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$
#### (2) $`A^{-1} = \begin{bmatrix} 0.4 & -0.7 \\ -0.1 & 0.4 \end{bmatrix}`$ 
#### (3) $`A^{-1} = \begin{bmatrix} 0.2 & -0.5 \\ -0.3 & 0.2 \end{bmatrix}`$
#### (4) $`A^{-1} = \begin{bmatrix} 0.1 & -0.3 \\ -0.2 & 0.4 \end{bmatrix}`$ 
#### (solution) (1) $`A^{-1} = \frac{1}{4\times6 - 7\times2}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \frac{1}{10}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$
