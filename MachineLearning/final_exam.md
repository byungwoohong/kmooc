### Question 1. What is the solution for the set of linear equations $` x_1 + x_2 = 1, x_1 = -1, x_1 - x_2 = 0 `$

#### (1) $` x_1 = -1, x_2 = 2 `$

#### (2) $` x_1 = -1, x_2 = -1 `$

#### (3) $` x_1 = 0, x_2 = 1 `$

#### (4) No solution

#### (solution) (4) No solution


### Question 2. What is the first order Talyor approximation of $` f(x) = e^x `$ at $` a `$?

#### (1) 1 + $` e^x `$

#### (2) 1 + $` e^a `$
 
#### (3) $` e^a +  e^a `$

#### (4) $` e^a + e^a(x-a) `$


#### (solution) (4) $` e^a + e^a(x-a) `$


### Question 3. Choose all the _wrong_ explanations related to the properties of matrix multiplication. Let A, B and C be the matrices, $`A^{T}`$ be the transposed matrix of matrix A, and $`I`$ be the Identity matrix.

#### (1) $`A I = A`$ and $`I A = A`$ 

#### (2) $`A^T (B + C) = A B^T + A C^T`$ 

#### (3) $`(A B)^{T} = B^{T} A^{T}`$ 

#### (4) $`A B = B^T A^T`$

#### (solution) (2) and (4) 


### Question 4. Compute the inverse of 2x2 matrix $` A = \begin{bmatrix} 4 & 7 \\ 2 & 6 \end{bmatrix}`$.

#### (1) $`A^{-1} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$
#### (2) $`A^{-1} = \begin{bmatrix} 0.4 & -0.7 \\ -0.1 & 0.4 \end{bmatrix}`$ 
#### (3) $`A^{-1} = \begin{bmatrix} 0.2 & -0.5 \\ -0.3 & 0.2 \end{bmatrix}`$
#### (4) $`A^{-1} = \begin{bmatrix} 0.1 & -0.3 \\ -0.2 & 0.4 \end{bmatrix}`$ 
#### (solution) (1) $`A^{-1} = \frac{1}{4\times6 - 7\times2}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \frac{1}{10}\begin{bmatrix}6&-7\\-2&4\end{bmatrix} = \begin{bmatrix} 0.6 & -0.7 \\ -0.2 & 0.4 \end{bmatrix}`$

### Question 5. Which of the followings is correct for the $`L`$-2 norm square?

#### (1) $`\lVert x \rVert^2 = x^T x`$

#### (2) $`\lVert x \rVert^2 = x x^T`$

#### (3) $`\lVert x \rVert^2 = x x`$

#### (4) $`\lVert x \rVert^2 = x^T x^T`$

#### (solution) (1) $`\lVert x \rVert^2 = x^T x`$


### Question 6. Let $`A`$ be a matrix and its columns are assumed to be linearly independent. Let $`\theta`$ be the unknown model parameters and $`y`$ be a given vector. Which of the followings is a solution $`\hat{\theta}`$ for the least square problem $`\lVert A\theta - y \rVert ^2`$?

#### (1) $`\hat \theta = A^{T}(A^{T}A)^{-1}y`$

#### (2) $`\hat \theta = y(A^{T}A)^{-1}A^{T}`$

#### (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$

#### (4) $`\hat \theta = A^{T}(AA^{T})^{-1}y`$

#### (solution) (3) $`\hat \theta = (A^{T}A)^{-1}A^{T}y`$



## Question 7. The code block below is to compute the logistic regression loss. Choose the best option to complete the code block, filling in the blank. 

A module ```compute_loss``` is to compute the logistic regression loss which is defined as $` L(w)=-\frac{1}{n} \sum_{i=1}^n \ \Big(  y_i \log(\sigma(w\cdot x_i)) + (1-y_i)\log(1-\sigma(w\cdot x_i)) \Big) `$ where $` \sigma(\eta) = \frac{1}{1+e^{-\eta}} `$, $` x `$ is the input data, $` y `$ is the ground truth, $` n `$ is the number of data and $` w `$ is the trainable model parameter (weight).

```python
# The length of x is assumed to be the same as the length of y.

import numpy

def compute_loss(x, w, y): # x and y are numpy arrays. w is an unknown scalar variable.
    n           =   y.shape[0]
    y_predict   =   1 / (1 +numpy.exp(-w*x))
    loss        = ### fill in the black ###

    return loss
```

#### (1) 
```python
    loss = 1/n* ( y.T.dot(np.log(y_predict)) + (1-y).T.dot(np.log(1-y_predict)) )
```

#### (2)
```python
    loss = -1/n* ( y.T.dot(np.log(y_predict)) + (1-y).T.dot(np.log(1-y_predict)) )
```

#### (3)
```python
    loss = -1/n* ( y_predict.T.dot(np.log(y)) + (1-y_predict).T.dot(np.log(1-y)) )
```

#### (4)
```python
    loss = 1/n* ( y_predict.T.dot(np.log(y)) + (1-y_predict).T.dot(np.log(1-y)) )
```

#### (solution) (2)
```python
    loss = -1/n* ( y.T.dot(np.log(y_predict)) + (1-y).T.dot(np.log(1-y_predict)) )
```

## Question 8. The code block below is to compute the gradient of the logistic regression loss. Choose the best option to complete the code block, filling in the blank.  

A module ```compute_gradient``` is to compute the logistic regression loss function which is defined as $` L(w)=-\frac{1}{n} \sum_{i=1}^n \ \Big(  y_i \log(\sigma(w\cdot x_i)) + (1-y_i)\log(1-\sigma(w\cdot x_i)) \Big) `$ where $` \sigma(\eta) = \frac{1}{1+e^{-\eta}} `$, $` x `$ is the input data, $` y `$ is the ground truth, $` n `$ is the number of data and $` w `$ is the trainable parameter (weight).

```python
# The length of x is assumed to be the same as the length of y.

import numpy

def compute_gradient(x, w, y): # x and y are numpy arrays. w is an unknown variable.
    n           =   y.shape[0]
    y_predict   =   1 / (1 +numpy.exp(-w*x))
    grad        = ### fill in the black ###

    return grad
```

#### (1) 
```python
    grad = -2/n* y.T.dot(y_predict-y)
```

#### (2)
```python
    grad = 2/n* y.T.dot(y_predict-y)
```

#### (3)
```python
    grad = -2/n* x.T.dot(y_predict-y)
```

#### (4)
```python
    grad = 2/n* x.T.dot(y_predict-y)
```

#### (solution) (4)
```python
    grad = 2/n* x.T.dot(y_predict-y)
```


## Question9. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.  

Let x be a numpy array of elements. The function is designed to compute **sigmoid** activation function. Find the right sigmoid expression among these four options.

```python
# variable x : numpy array 
# Sigmoid function is based on the sigmoid equation

import numpy as np

def sigmoid(x):
    sigmoid_x   = ## fill in the blank ##
    return sigmoid_x

```

#### (1) 
```python
    sigmoid_x = 1 / (1 + np.exp(x))
```

#### (2)
```python
    sigmoid_x = 1 / (1 + np.exp(-x))  
```

#### (3)
```python
    sigmoid_x = 1 / (x + np.exp(-x))
```

#### (4)
```python
    sigmoid_x = 1 / (x + np.exp(x))
```

#### Solution (2)
```python
    sigmoid_x = 1 / (1 + np.exp(-x))  
```

## Question10. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.  

Let x be a numpy array of elements. Compute the **derivative of the sigmoid** activation function and find the expression among these four options. 

```python
# variable "x" : input array 
# variable "result" : the result of derivative of the sigmoid function.

import numpy as np

def derivative_of_sigmoid(x):
    sigmoid     = 1 / (1 + np.exp(-x))
    result      = ## fill in the blank ##
    return result
```

#### (1) 
```python
    result = sigmoid * (1-sigmoid)  
```

#### (2)
```python
    result = sigmoid * (1+sigmoid)
```

#### (3)
```python
    result = sigmoid / (1-sigmoid)
```

#### (4)
```python
    result = sigmoid / (1*sigmoid)
```

#### Solution (1)
```python
    result = sigmoid * (1-sigmoid)  
```
