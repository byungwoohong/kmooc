# Week 09

### Question 1. Let $`w = (w_1, w_2, \cdots, w_n) \in \mathbb{R}^n`$ be a vector for the model parameters in a neural network. Which of the followings is the expression for the regularization loss based on the squared $`L_2`$-norm.

#### (1) $`w_1 + w_2 + \cdots + w_n`$

#### (2) $`|w_1| + |w_2| + \cdots + |w_n|`$

#### (3) $`(w_1 + w_2 + \cdots + w_n)^2`$

#### (4) $`w_1^2 + w_2^2 + \cdots + w_n^2`$

#### (solution) (4)

### Question 2. Let $`\mathcal{R}(w)`$ be a loss for the regularization and $`\lambda`$ be its associated control parameter leading to the regularization term $`\lambda \, \mathcal{R}(w)`$. Choose all the correct statements in the followings.

#### (1) It is desired to increase $`\lambda`$ when over-fitting occurs 

#### (2) It is desired to decrease $`\lambda`$ when over-fitting occurs 

#### (3) It is desired to increase $`\lambda`$ when under-fitting occurs 

#### (4) It is desired to decrease $`\lambda`$ when under-fitting occurs

#### (solution) (1) and (4)