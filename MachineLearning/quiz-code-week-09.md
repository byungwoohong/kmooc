# [Week 09] (Model Generation)

## Unit09-1. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.

When the Loss function with L2 Regularization is 
$` L(\theta)=\frac{1}{2m}\sum (\hat{y_{\theta}} - y )^2 + \lambda \sum \theta^2 `$,

$`m`$ is the number of data, 
$`\hat{y_{\theta}}`$ is the predicted result, $`y`$ is the ground truth and $`\theta`$ is the parameter to find.

The function **L2** is designed to compute the loss function with L2-norm square(regularization term).
```python
# theta is a parameter of the model.
# y_true is ground truth and y_pred is predicted result.

import numpy as np

def L2(y_true, y_pred, theta, labmda):
 
    total_num   =   y_true.shape[0]
    
    #data fidelity term
    fidelity = 1 / (2 * y_pred.shape[0]) * np.sum((y_pred - y_true)**2) 
    
    # L2 regularization term
    regularization = ## fill in the blank ##
    
    return fidelity + regularization
```

#### (1) 
```python
    regularization = labmda * np.sum(theta)
```

#### (2)
```python
    regularization = labmda * np.sum(np.abs(theta))
```

#### (3)
```python
    regularization = labmda * np.sum(theta**2)
```

#### (4)
```python
    regularization = labmda * (np.sum(theta)**2)

```

#### (solution) (3)
```python
    regularization = labmda * np.sum(theta**2)
```


## Unit09-2. Choose the best option for filling in the blanks in the following code blocks to achieve the specified functionality.
When the gradient of loss function with L2 Regularization is
$` L(\theta)=\frac{1}{2m}\sum (\hat{y_{\theta}} - y  )^2 + \lambda \sum \theta^2 `$,

$`m`$ is the number of data, 
$`\hat{y_{\theta}}`$ is the predicted result, $`y`$ is the ground truth and $`\theta`$ is a parameter to find.

Find the derivative of theta with respect to the loss function, and fill in the blank.



The function **grad_L2** is designed to compute the gradient of loss function with L2-norm square(regularization term).

```python

# theta is parameters of the model.

import numpy as np

def grad_L2(x, y_true, y_pred, theta, labmda):
 
    total_num   =   y_true.shape[0]
    
    #gradient of data fidelity term
    grad_fidelity = 1 / (2 * y_pred.shape[0]) * np.sum((y_pred - y_true).reshape(-1,1)*x) 
    
    #gradient of L2 regularization term
    grad_regularization = ## fill in the blank ##
    
    return grad_fidelity + grad_regularization
```

#### (1) 
```python
    grad_regularization = 2 * labmda * theta
```

#### (2)
```python
    grad_regularization = 2 * labmda * (theta**2)
```

#### (3)
```python
    grad_regularization = 2 * labmda
```

#### (4)
```python
    grad_regularization = 2 * labmda * np.sum(theta)

```

#### (solution) (1)
```python
    grad_regularization = 2 * labmda * theta
```