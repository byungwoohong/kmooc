# [Week 13] (Neural Network)

Let $`x\in \mathbf{R}^{depth\times height\times width}`$ be observed data and $`F \in \mathbf{R}^{filter\_depth\times filter\_height\times filter\_width}`$ be a convolutional filter of a convolutional layer. Say that a ReLU function is adopted as an activation function for the layer. The following Python code snippet defines a convolutional connected layer class and a forward process of the layer with a blank to be completed, setting $`x\in \mathbf{R}^{3\times 32\times 32}`$ and $`F \in \mathbf{R}^{3\times 3\times 3}`$. It is assumed that bias is not used and the size of stride of convolutional filters is fixed as 1.

```python
import numpy as np

class ConvolutionalLayer(object):
    def __init__(self, filter_num, filter_depth, filter_height, filter_width, stride=1):
        self.filter_num = filter_num
        self.filter_depth = filter_depth
        self.filter_height = filter_height
        self.filter_width = filter_width
        self.stride = stride
        self.filters = np.zeros((self.filter_num, self.filter_depth, self.filter_height, self.filter_width))

    def initialize_random_conv_filter(self, mu, sigma):
        self.filters = np.random.normal(mu, sigma, size=(self.filter_num, self.filter_depth, self.filter_height, self.filter_width)) # solution
        return

    def convolution(self, x):
        """
        convolution operation of convolutional layer on data x with convolutional filters with stride  
        """
        input_channel, input_height, input_width = x.shape
        output_height = int((input_height - self.filter_height)/self.stride) + 1
        output_width = int((input_width - self.filter_width)/self.stride) + 1
        conv_output = np.zeros((self.filter_num, output_height, output_width))
        # convolution
        for f in range(self.filter_num):
            r_curr = r_out = 0
            while r_curr + self.filter_height <= input_height:
                c_curr = c_out = 0
                while c_curr + self.filter_width <= input_width:
                    conv_output[f, r_out, c_out] = #-----blank-----#
                    c_curr += self.stride
                    c_out += 1
                r_curr += self.stride
                r_out += 1
        return conv_output    
  
    def forward_pass(self, x):
        conv_output = self.convolution(x)
        output = ReLU(conv_output)
        return output
    
def ReLU(x):
    return np.clip(x, 0, np.inf)

if __name__=="__main__":
    x = np.random.randint(0, 10, size=(3, 10, 10))
    filter_num, filter_depth, filter_height, filter_width = 8, 3, 3, 3
    mu, sigma = 0, 0.1
    conv_layer = ConvolutionalLayer(num_f, k_d, k_h, k_w, stride=1) # init conv layer
    conv_layer.initialize_random_conv_filter(mu, sigma) # init weight values of convolutional filter conv layer
    y = conv_layer.forward_pass(x) # get outputs from the conv layer
```

## Unit14-1. Given the code snippet above, which one best compute the convolution of the convolutional filters and the input data, filling in the blank in a module ```def convolution```? 


#### (1) 
```python
np.sum(self.filters[f] + x[:, r_curr:r_curr+self.filter_height, c_curr:c_curr+self.filter_width])
```

#### (2)
```python
np.sum(self.filters[f] * x[:, r_curr:r_curr+self.filter_height, c_curr:c_curr+self.filter_width])
```

#### (3)
```python
np.amax(self.filters[f] + x[:, r_curr:r_curr+self.filter_height, c_curr:c_curr+self.filter_width])
```

#### (4)
```python
np.amax(self.filters[f] * x[:, r_curr:r_curr+self.filter_height, c_curr:c_curr+self.filter_width])
```

#### (solution) (2)
```python
np.sum(self.filters[f] * x[:, r_curr:r_curr+self.filter_height, c_curr:c_curr+self.filter_width])
```

## Unit14-2. Given the code snippet above, what will be the shape of the final output ```y``` generated from the convolutional layer?


#### (1) 
$`8 \times 32 \times 32`$

#### (2)
$`8 \times 26 \times 26`$

#### (3)
$`8 \times 30 \times 30`$

#### (4)
$`8 \times 28 \times 28`$

#### (solution) (3)
$`8 \times 30 \times 30`$