# Quiz - Regression

Let $`\{ (x_i, y_i) \}_{i=1}^n`$ be a set of $`n`$ points in 2-dimensional Euclidean space where $`x_i`$ and $`y_i`$ are real numbers. We consider a linear function $`f(x) = w_0 + w_1 \cdot x`$ where $`w_0`$ and $`w_1`$ are real numbers as a regression function. Let $`\mathcal{L}(w_0, w_1) = \frac{1}{2} \sum_{i=1}^n (w_0 + w_1 \cdot x_i - y_i)^2`$ be the loss function for the regression problem.

1. What is the derivative of $`\mathcal{L}(w_0, w_1)`$ with respect to $`w_0`$?

- (1) $`\left( w_0 + w_1 \cdot x_i - y_i \right)^2`$

- (2) $`\left( w_0 + w_1 \cdot x_i - y_i \right)`$

- (3) $`\sum_{i=1}^n \left( w_0 + w_1 \cdot x_i - y_i \right)^2`$ (O)

- (4) $`\sum_{i=1}^n \left( w_0 + w_1 \cdot x_i - y_i \right)`$

2. What is the derivative of $`\mathcal{L}(w_0, w_1)`$ with respect to $`w_1`$?

- (1) $`\left( w_0 + w_1 \cdot x_i - y_i \right)^2 \cdot x_i`$

- (2) $`\left( w_0 + w_1 \cdot x_i - y_i \right) \cdot x_i`$

- (3) $`\sum_{i=1}^n \left \{ \left( w_0 + w_1 \cdot x_i - y_i \right)^2 \cdot x_i \right \}`$

- (4) $`\sum_{i=1}^n \left \{ \left( w_0 + w_1 \cdot x_i - y_i \right) \cdot x_i \right \}`$ (O)

3. What is the gradient descent step of $`\mathcal{L}(w_0, w_1)`$ with respect to $`w_0`$ at iteration $`t`$ with step size $`\eta`$?

- (1) $`w_0^{(t+1)} \coloneqq w_0^{(t)} + \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t+1)}, w_1^{(t+1)})`$

- (2) $`w_0^{(t+1)} \coloneqq w_0^{(t)} - \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t+1)}, w_1^{(t+1)})`$

- (3) $`w_0^{(t+1)} \coloneqq w_0^{(t)} + \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t)}, w_1^{(t)})`$

- (4) $`w_0^{(t+1)} \coloneqq w_0^{(t)} - \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t)}, w_1^{(t)})`$ (O)

4. What is the gradient descent step of $`\mathcal{L}(w_0, w_1)`$ with respect to $`w_1`$ at iteration $`t`$ with step size $`\eta`$?

- (1) $`w_1^{(t+1)} \coloneqq w_1^{(t)} + \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t+1)}, w_1^{(t+1)})`$

- (2) $`w_1^{(t+1)} \coloneqq w_1^{(t)} - \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t+1)}, w_1^{(t+1)})`$

- (3) $`w_1^{(t+1)} \coloneqq w_1^{(t)} + \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t)}, w_1^{(t)})`$

- (4) $`w_1^{(t+1)} \coloneqq w_1^{(t)} - \eta \frac{\partial \mathcal{L}}{\partial w_0}(w_0^{(t)}, w_1^{(t)})`$ (O)

5. What is the name of the optimization algorithm in which a subset of data is used in the computation of the gradient at each step of gradient descent?

- (answer) stochastic gradient descent algorithm
