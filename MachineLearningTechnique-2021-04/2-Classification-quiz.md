# Quiz - Classification

Let $`\{ (p_i, l_i) \}_{i=1}^n`$ be a set of training data where $`p_i = (x_i, y_i) \in \mathbb{R}^2`$ is a point in 2-dimensional Euclidean space and $`l_i \in \{ 0, 1 \}`$ is a label for point $`p_i`$. We consider a function $`h(p) = \sigma( f(p) )`$ as a classifier where $`\sigma(z) = \frac{1}{1 + \exp{(-z)}}`$ and $`f(p) = w_0 + w_1 \cdot x + w_2 \cdot y `$ where $`w_0, w_1, w_2`$ are real numbers. Let us denote by $`\mathcal{L}(w_0, w_1, w_2)`$ the loss function for the classification problem.

1. What is the most appropiate loss function for $`(p_i, l_i)`$ when $`l_i = 0`$?

- (1) $`h(p_i)`$

- (2) $`1 - h(p_i)`$

- (3) $`- \log{(h(p_i))}`$

- (4) $`- \log{(1 - h(p_i))}`$ (O)

2. What is the most appropiate loss function for $`(p_i, l_i)`$ when $`l_i = 1`$?

- (1) $`h(p_i)`$

- (2) $`1 - h(p_i)`$

- (3) $`- \log{(h(p_i))}`$ (O)

- (4) $`- \log{(1 - h(p_i))}`$

3. What is the derivative of $`\sigma(z)`$ with respect to $`z`$?

- (1) $`\sigma(z)`$

- (2) $`\sigma(z) (1 - \sigma(z))`$ (O)

- (3) $`\sigma(z) (\sigma(z) - 1)`$

- (4) $`(1- \sigma(z))(\sigma(z) - 1)`$

4. What is the derivative of $`\log{(\sigma(z))}`$ with respect to $`z`$?

- (1) $`\sigma(z)`$

- (2) $`- \sigma(z)`$

- (3) $`(1 - \sigma(z))`$ (O)

- (4) $`(\sigma(z) - 1)`$

5. Is the logistic regression loss convex? 

- (answer) Yes
